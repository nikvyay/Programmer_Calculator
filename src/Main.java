import java.io.IOException;
import java.util.Scanner;
public class Main
{
    public static void main(String[] args) throws IOException {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Choose conversion type:");
            System.out.println("1. Binary");
            System.out.println("2. Octal");
            System.out.println("3. Decimal");
            System.out.println("4. Hexadecimal");
            System.out.println("0. Exit");
            int choicetype = scanner.nextInt();

            switch (choicetype) {
                case 0:
                    return;
                default:
                    System.out.println("Invalid choice");
                    break;
            }

            Scanner scanner3 = new Scanner(System.in);
            System.out.println("Choose operation:");
            System.out.println("1. Plus");
            System.out.println("2. Minus");
            System.out.println("3. Split");
            System.out.println("4. Multiply");
            System.out.println("5. Percent");
            int choiceop = scanner3.nextInt();

            Scanner scanner1 = new Scanner(System.in);
            System.out.print("Enter a number 1: ");
            String number1 = scanner1.nextLine();

            Scanner scanner2 = new Scanner(System.in);
            System.out.print("Enter a number 2: ");
            String number2 = scanner2.nextLine();

            switch (choicetype) {
                case 1:
                    Calculator.vivodBinary(number1, number2, choiceop);
                    break;
                case 2:
                    Calculator.vivodOctal(number1, number2, choiceop);
                    break;
                case 3:
                    Calculator.vivodDecimal(number1, number2, choiceop);
                    break;
                case 4:
                    Calculator.vivodHex(number1, number2, choiceop);
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;
            }
        }
    }
}