
abstract public class Calculator
{
    public static boolean  flag = false;
    protected static int cal = 0;

    private static int binaryToDecimal(String binary) {return Integer.parseInt(binary, 2);}

    private static int octalToDecimal(String octal) {return Integer.parseInt(octal, 8);}

    private static int hexToDecimal(String hex) {
        return Integer.parseInt(hex, 16);
    }

    private static String decimalToBinary(int decimal) {
        return Integer.toBinaryString(decimal);
    }

    private static String decimalToOctal(int decimal) {
        return Integer.toOctalString(decimal);
    }

    private static String decimalToHex(int decimal) {
        return Integer.toHexString(decimal);
    }

    private static int calculate(int number1, int number2, int choiceop) {

        switch (choiceop) {
            case 1:
                new Plus(number1, number2);
                break;
            case 2:
                new Minus(number1, number2);
                break;
            case 3:
                new Split(number1, number2);
                break;
            case 4:
                new Multiply(number1, number2);
                break;
            case 5:
                new Percent(number1, number2);
                break;
        }
        return cal;
    }

    public static void vivodBinary(String number1, String number2, int choiceop) {
        if (number1.matches("[01]+") && number2.matches("[01]+"))
        {
            calculate(binaryToDecimal(number1),binaryToDecimal(number2),choiceop);
            if (flag == false)
            {
                System.out.println("Binary: " + decimalToBinary(calculate(binaryToDecimal(number1),binaryToDecimal(number2),choiceop)));
                System.out.println("Octal: " + decimalToOctal(calculate(binaryToDecimal(number1),binaryToDecimal(number2),choiceop)));
                System.out.println("Decimal: " + calculate(binaryToDecimal(number1),binaryToDecimal(number2),choiceop));
                System.out.println("Hexadecimal: " + decimalToHex(calculate(binaryToDecimal(number1),binaryToDecimal(number2),choiceop)));
            }
            else
            {
                System.out.println("Делить на ноль нельзя!");
                flag = false;
            }
        }
        else
        {
            System.out.println("Введите чило состоящее из 1 и 0");
        }
    }
    public static void vivodOctal(String number1, String number2, int choiceop) {
        if (number1.matches("[0-7]+") && number2.matches("[0-7]+"))
        {
            calculate(octalToDecimal(number1),octalToDecimal(number2),choiceop);
            if (flag == false) {
                System.out.println("Binary: " + decimalToBinary(calculate(octalToDecimal(number1), octalToDecimal(number2), choiceop)));
                System.out.println("Octal: " + decimalToOctal(calculate(octalToDecimal(number1), octalToDecimal(number2), choiceop)));
                System.out.println("Decimal: " + calculate(octalToDecimal(number1), octalToDecimal(number2), choiceop));
                System.out.println("Hexadecimal: " + decimalToHex(calculate(octalToDecimal(number1), octalToDecimal(number2), choiceop)));
            }
            else
            {
                System.out.println("Делить на ноль нельзя!");
                flag = false;
            }
        }
        else
        {
            System.out.println("Введите чило состоящее от 0 до 7");
        }
    }
    public static void vivodDecimal(String number1, String number2, int choiceop) {
        if (number1.matches("[0-9]+") && number2.matches("[0-9]+"))
        {
            calculate(Integer.parseInt(number1),Integer.parseInt(number2),choiceop);
            if (flag == false) {
                System.out.println("Binary: " + decimalToBinary(calculate(Integer.parseInt(number1), Integer.parseInt(number2), choiceop)));
                System.out.println("Octal: " + decimalToOctal(calculate(Integer.parseInt(number1), Integer.parseInt(number2), choiceop)));
                System.out.println("Decimal: " + calculate(Integer.parseInt(number1), Integer.parseInt(number2), choiceop));
                System.out.println("Hexadecimal: " + decimalToHex(calculate(Integer.parseInt(number1), Integer.parseInt(number2), choiceop)));
            }
            else
            {
                System.out.println("Делить на ноль нельзя!");
                flag = false;
            }
        }
        else
        {
            System.out.println("Введите чило состоящее от 0 до 9");
        }
    }
    public static void vivodHex(String number1, String number2, int choiceop) {
        if (number1.matches("[0-9a-f]+") && number2.matches("[0-9a-f]+"))
        {
            calculate(hexToDecimal(number1),hexToDecimal(number2),choiceop);
            if (flag == false) {
                System.out.println("Binary: " + decimalToBinary(calculate(hexToDecimal(number1), hexToDecimal(number2), choiceop)));
                System.out.println("Octal: " + decimalToOctal(calculate(hexToDecimal(number1), hexToDecimal(number2), choiceop)));
                System.out.println("Decimal: " + calculate(hexToDecimal(number1), hexToDecimal(number2), choiceop));
                System.out.println("Hexadecimal: " + decimalToHex(calculate(hexToDecimal(number1), hexToDecimal(number2), choiceop)));
            }
            else
            {
                System.out.println("Делить на ноль нельзя!");
                flag = false;
            }
        }
        else
        {
            System.out.println("Введите чило состоящее от 0 до 9 или из букв от a до f");
        }
    }
}
